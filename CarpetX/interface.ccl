# Interface definition for thorn CarpetX

IMPLEMENTS: CarpetX

INHERITS: IO

USES INCLUDE HEADER: silo.hxx



USES INCLUDE HEADER: rational.hxx
USES INCLUDE HEADER: vect.hxx

INCLUDES HEADER: loop.hxx IN loop.hxx
INCLUDES HEADER: loopcontrol.h IN loopcontrol.h



# The overall size of the domain
CCTK_INT FUNCTION GetDomainSpecification
  (CCTK_INT IN size,
   CCTK_REAL OUT ARRAY physical_min,
   CCTK_REAL OUT ARRAY physical_max,
   CCTK_REAL OUT ARRAY interior_min,
   CCTK_REAL OUT ARRAY interior_max,
   CCTK_REAL OUT ARRAY exterior_min,
   CCTK_REAL OUT ARRAY exterior_max,
   CCTK_REAL OUT ARRAY spacing)
PROVIDES FUNCTION GetDomainSpecification WITH CarpetX_GetDomainSpecification LANGUAGE C

# Convenient way to determine boundary sizes
CCTK_INT FUNCTION GetBoundarySizesAndTypes
  (CCTK_POINTER_TO_CONST IN cctkGH,
   CCTK_INT IN size,
   CCTK_INT OUT ARRAY bndsize,
   CCTK_INT OUT ARRAY is_ghostbnd,
   CCTK_INT OUT ARRAY is_symbnd,
   CCTK_INT OUT ARRAY is_physbnd)
PROVIDES FUNCTION GetBoundarySizesAndTypes WITH CarpetX_GetBoundarySizesAndTypes LANGUAGE C

void FUNCTION GetTileExtent(
  CCTK_POINTER_TO_CONST IN cctkGH,
  CCTK_INT ARRAY OUT tile_min,
  CCTK_INT ARRAY OUT tile_max)
PROVIDES FUNCTION GetTileExtent WITH CarpetX_GetTileExtent LANGUAGE C

void FUNCTION GetLoopBoxAll(
  CCTK_POINTER_TO_CONST IN cctkGH,
  CCTK_INT IN size,
  CCTK_INT ARRAY OUT loop_min,
  CCTK_INT ARRAY OUT loop_max)
PROVIDES FUNCTION GetLoopBoxAll WITH CarpetX_GetLoopBoxAll LANGUAGE C

void FUNCTION GetLoopBoxInt(
  CCTK_POINTER_TO_CONST IN cctkGH,
  CCTK_INT IN size,
  CCTK_INT ARRAY OUT loop_min,
  CCTK_INT ARRAY OUT loop_max)
PROVIDES FUNCTION GetLoopBoxInt WITH CarpetX_GetLoopBoxInt LANGUAGE C



void FUNCTION CallScheduleGroup(
  CCTK_POINTER IN cctkGH,
  CCTK_STRING IN groupname)
PROVIDES FUNCTION CallScheduleGroup WITH CarpetX_CallScheduleGroup LANGUAGE C

void FUNCTION SolvePoisson(
  CCTK_INT IN gi_sol,
  CCTK_INT IN gi_rhs,
  CCTK_INT IN gi_res,
  CCTK_REAL IN reltol,
  CCTK_REAL IN abstol,
  CCTK_REAL OUT res_initial,
  CCTK_REAL OUT res_final)
PROVIDES FUNCTION SolvePoisson WITH CarpetX_SolvePoisson LANGUAGE C

void FUNCTION Interpolate(
  CCTK_POINTER_TO_CONST IN cctkGH,
  CCTK_INT IN npoints,
  CCTK_REAL ARRAY IN coordsx,
  CCTK_REAL ARRAY IN coordsy,
  CCTK_REAL ARRAY IN coordsz,
  CCTK_INT IN nvars,
  CCTK_INT ARRAY IN varinds,
  CCTK_INT ARRAY IN operations,
  CCTK_POINTER IN resultptrs)
PROVIDES FUNCTION Interpolate WITH CarpetX_Interpolate LANGUAGE C


# Interpolation
#TODO: This will be needed for interface with SymmetryInterpolate
#CCTK_INT FUNCTION SymmetryInterpolate(
#  CCTK_POINTER_TO_CONST IN cctkGH,
#  CCTK_INT IN N_dims,
#  CCTK_INT IN local_interp_handle,
#  CCTK_INT IN param_table_handle,
#  CCTK_INT IN coord_system_handle,
#  CCTK_INT IN N_interp_points,
#  CCTK_INT IN interp_coords_type,
#  CCTK_POINTER_TO_CONST ARRAY IN interp_coords,
#  CCTK_INT IN N_input_arrays,
#  CCTK_INT ARRAY IN input_array_indices,
#  CCTK_INT IN N_output_arrays,
#  CCTK_INT ARRAY IN output_array_types,
#  CCTK_POINTER ARRAY IN output_arrays)
#USES FUNCTION SymmetryInterpolate

CCTK_INT FUNCTION DriverInterpolate(
  CCTK_POINTER_TO_CONST IN cctkGH,
  CCTK_INT IN N_dims,
  CCTK_INT IN local_interp_handle,
  CCTK_INT IN param_table_handle,
  CCTK_INT IN coord_system_handle,
  CCTK_INT IN N_interp_points,
  CCTK_INT IN interp_coords_type,
  CCTK_POINTER_TO_CONST ARRAY IN interp_coords,
  CCTK_INT IN N_input_arrays,
  CCTK_INT ARRAY IN input_array_indices,
  CCTK_INT IN N_output_arrays,
  CCTK_INT ARRAY IN output_array_types,
  CCTK_POINTER ARRAY IN output_arrays)
PROVIDES FUNCTION DriverInterpolate WITH CarpetX_DriverInterpolate LANGUAGE C

PUBLIC:

CCTK_REAL regrid_error TYPE=gf
{
  regrid_error
} "Regridding condition"

CCTK_REAL refinement_level TYPE=gf
{
  refinement_level
} "Refinement level"
